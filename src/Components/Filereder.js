import React, { Component } from "react";
import CSVReader from "react-csv-reader";

class FileReader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      selectedCompany: "None",
    };
  }
  handleForce = (data) => {
    this.setState({ data });
  };
  handleChange = (e) => {
    console.log(this.state);
    this.setState({ selectedCompany: e.target.value });
  };

  render() {
    const list = this.state.data;
    let company = this.state.data.filter((company) => {
        console.log(company.manager,"company.manager");
      return company.manager === this.state.selectedCompany;
      
    });
    console.log(company,"company");
    const papaparseOptions = {
      header: true,
      dynamicTyping: true,
      skipEmptyLines: true,
      transformHeader: (header) => header.toLowerCase().replace(/\W/g, "_"),
    };

 

    return (
      <div>
        <CSVReader
          cssClass="csv-reader-input"
          label="Select CSV with secret Death Star statistics"
          onFileLoaded={this.handleForce}
          onError={this.handleDarkSideForce}
          parserOptions={papaparseOptions}
          inputId="ObiWan"
          inputName="ObiWan"
          inputStyle={{ color: "red" }}
        />

        <div>
          {list.length ? (
            <div>
              <div>
                <table>
                  <tr>
                    <th>CEO <br/>
                    <select
                  value={this.state.selectedCompany}
                  onChange={this.handleChange.bind(this)}
                >
                  {this.state.data.map((company, i) => {
                    return <option>{company.ceo}</option>;
                  })}
                </select></th>
                    <th>Manager <br/>
                    <select
                  value={this.state.selectedCompany}
                  onChange={this.handleChange.bind(this)}
                >
                  {this.state.data.map((company, i) => {
                    return <option>{company.manager}</option>;
                  })}
                </select></th>
                    <th>Supervisor <br/>
                    <select
                  value={this.state.selectedCompany}
                  onChange={this.handleChange.bind(this)}
                >
                  {this.state.data.map((company, i) => {
                    return <option>{company.supervisor}</option>;
                  })}
                </select></th>
                    <th>Employee <br/>
                    <select
                  value={this.state.selectedCompany}
                  onChange={this.handleChange.bind(this)}
                >
                  {this.state.data.map((company, i) => {
                    return <option>{company.employee}</option>;
                  })}
                </select></th>
                  </tr>
                  {list.map((item) => (
                    <tr>
                      <td>{item.ceo}</td>
                      <td>{item.manager}</td>
                      <td>{item.supervisor}</td>
                      <td>{item.employee}</td>
                    </tr>
                  ))}
                </table>
              </div>
            </div>
          ) : (
            <p>No Records</p>
          )}
        </div>
      </div>
    );
  }
}

export default FileReader;
